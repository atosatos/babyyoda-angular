import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BookComponent } from './book/book.component';
import { HomeComponentComponent } from './home-component/home-component.component';
import {MenuComponentComponent} from './menu-component/menu-component.component'
import {HearingComponentComponent} from './hearing-component/hearing-component.component';

const routes: Routes = [
  {path:'',component:MenuComponentComponent},
{path:'home',component:HomeComponentComponent},
{path:'book',component:BookComponent},
{path:'hearing', component: HearingComponentComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
