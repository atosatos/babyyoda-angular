import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BookComponent } from './book/book.component';
import { HomeComponentComponent } from './home-component/home-component.component';
import { MenuComponentComponent } from './menu-component/menu-component.component';
import { HearingComponentComponent } from './hearing-component/hearing-component.component';

@NgModule({
  declarations: [
    AppComponent,
    BookComponent,
    HomeComponentComponent,
    MenuComponentComponent,
    HearingComponentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    CommonModule,
    RouterModule.forRoot([
      {path: '', redirectTo: '/book', pathMatch: 'full'},
      {path: 'book', component: BookComponent},


    ]),
  ],

  providers: [],
  bootstrap: [AppComponent],

})
export class AppModule { }
