import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Book } from 'src/models/book';
import { BookService } from 'src/services/book.service';
import { FormsModule, NgForm } from '@angular/forms';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { ThrowStmt } from '@angular/compiler';



@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {
  httpOptions = {
    headers: new HttpHeaders({

      'content-type': 'application/json'

    })
  };

  constructor(protected router: Router,private bookService: BookService,private http: HttpClient) {

  }


  lists: Array<Book> =
  [];
  page: any = 1;
  pageSize: any = 5;
  pageLimit: any = 0;
  submitted = false;

  ngOnInit(): void {
    setInterval(() => {
      this.page++;
      
      if(this.page > this.pageLimit) {
        this.page = 1;
      }
    }, 3000);
  }

 
  onSubmit() { this.submitted = false; }

  model = new Book('Test',0,'','');


  login(){
    this.bookService.getFullList().subscribe((u)=>{
      console.log(u);
      this.lists = u;
      this.pageLimit = this.lists.length / this.pageSize;
    });
  }

  save(f:NgForm){

    this.model = new Book(this.model.bookName.toString(),+this.model.price.toString(),this.model.category.toString(), this.model.author.toString());
    console.log(this.model);
    this.bookService.saveBook(this.model).subscribe(u=>{console.log(u)});

  }

}
