import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HearingComponentComponent } from './hearing-component.component';

describe('HearingComponentComponent', () => {
  let component: HearingComponentComponent;
  let fixture: ComponentFixture<HearingComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HearingComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HearingComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
