import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Book } from 'src/models/book';
import { BookService } from 'src/services/book.service';
import { Hearing } from 'src/models/hearing';
import { HearingService } from 'src/services/hearing.service';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';

@Component({
  selector: 'app-hearing-component',
  templateUrl: './hearing-component.component.html',
  styleUrls: ['./hearing-component.component.css']
})
export class HearingComponentComponent implements OnInit {

  date:Date;
  httpOptions = {
    headers: new HttpHeaders({

      'content-type': 'application/json'

    })
  };

  constructor(protected router: Router, private hearingService: HearingService, private http: HttpClient) {
    setInterval(() => {
      this.date = new Date()
    }, 1000);
  }

  title: String = "Raspored suđenja";
  lists: Array<Hearing> =
  [];
  page: any = 1;
  pageSize: any = 5;
  pageLimit: any = 0;

  ngOnInit(): void {
    this.hearingService.getHearings().subscribe((u)=>{
      this.lists = u;
      this.pageLimit = Math.ceil(this.lists.length / this.pageSize);
    });
    setInterval(() => {
      this.page++;
      if(this.page > this.pageLimit) {
        this.page = 1;
      }
    }, 3000);
  }

}
