export class Book {
  constructor(
    public bookName: string,
    public price: number,
    public category :string,
    public author: string,

  ) {  }

}
