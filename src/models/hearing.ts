export class Hearing {
    constructor(
      public caseNum: string,
      public hearingType: string,
      public hearingLocation :string,
      public date: string,
  
    ) {  }
  
  }
  