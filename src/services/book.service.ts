import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Book } from 'src/models/book';
import { ResourseServiceService } from './resourse-service.service';


@Injectable({
  providedIn: 'root'
})
export class BookService extends ResourseServiceService<Book>{
  getResourceUrl(): string {
    return 'books';
  }

  constructor(protected httpClient: HttpClient) {
    super(httpClient);
  }

  saveBook(book: Book){
    const headers = { 'content-type': 'application/json'}
    const body=JSON.stringify(book);
    console.log(this.getResourceUrl);
    return this.httpClient.post('http://localhost:5000/api/books',book,{'headers':headers})
  }
}
