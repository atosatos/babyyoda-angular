import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Hearing  } from 'src/models/hearing';
import { ResourseServiceService } from './resourse-service.service';

@Injectable({
  providedIn: 'root'
})
export class HearingService extends ResourseServiceService<Hearing>{
  getResourceUrl(): string {
    return 'hearings';
  }

  constructor(protected httpClient: HttpClient) {
    super(httpClient);
  }

}