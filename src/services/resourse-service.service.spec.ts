import { TestBed } from '@angular/core/testing';

import { ResourseServiceService } from './resourse-service.service';

describe('ResourseServiceService', () => {
  let service: ResourseServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ResourseServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
