import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable,throwError } from 'rxjs'
import { catchError } from 'rxjs/operators';
import {environment} from '../environments/environment';


@Injectable({
  providedIn: 'root'
})
export abstract class ResourseServiceService<T> {

  private readonly APIUrl = environment.api + this.getResourceUrl();
   httpOptions = {
    headers: new HttpHeaders({

      'content-type': 'application/json'

    })
  };

  constructor(protected httpClient: HttpClient) { }

  abstract getResourceUrl(): string;

  getFullList() : Observable<T[]>{
    console.log("API:" + this.APIUrl);
    return this.httpClient.get<T[]>(this.APIUrl)
      .pipe(
        catchError(this.handleError)
      );
  }

  getHearings() : Observable<T[]>{
    console.log("API:" + this.APIUrl);
    return this.httpClient.get<T[]>(this.APIUrl)
      .pipe(
        catchError(this.handleError)
      );
  }

  postData(data:T){
    const headers = { 'content-type': 'application/json', 'Access-Control-Allow-Origin':'*'}
    console.log("JSON: "+ JSON.stringify(data) +"\n url: "+ this.APIUrl+"\n\n DATA: "+ {data});
    return this.httpClient.post<T>(this.APIUrl,data,this.httpOptions);

  }

  private handleError(error: HttpErrorResponse) {
    // Handle the HTTP error here
    return throwError('Something wrong happened');
  }

}
